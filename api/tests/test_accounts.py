from fastapi.testclient import TestClient
from main import app
from models.accounts import AccountOut
from authenticator import authenticator
from fastapi import Request

client = TestClient(app)


def test_get_token_with_valid_account():
    mock_account = AccountOut(
        id=1, username="testuser", first_name="", last_name=""
    )
    mock_token = "mock-access-token"

    def mock_try_get_current_account_data(request: Request) -> AccountOut:
        return mock_account

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_try_get_current_account_data

    client.cookies[authenticator.cookie_name] = mock_token

    response = client.get("/token")

    app.dependency_overrides.clear()

    assert response.status_code == 200
    response_json = response.json()
    assert response_json["access_token"] == mock_token
    assert response_json.get("token_type") == "Bearer"
    assert response_json["account"] == mock_account


def test_get_token_with_invalid_account():
    def mock_try_get_current_account_data(request: Request) -> AccountOut:
        return None

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_try_get_current_account_data

    response = client.get("/token")

    app.dependency_overrides.clear()

    assert response.status_code == 200
    response_json = response.json()
    assert response_json is None

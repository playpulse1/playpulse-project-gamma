from fastapi import FastAPI, Query
import requests
from dotenv import load_dotenv
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import accounts
import os

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }


load_dotenv()


STEAM_API_KEY = "8CF9FA3D0E4142DCB074C5352C9FF7DF"


@app.get("/")
def read_root():
    return {"message": "Welcome to the Steam App List API"}


@app.get("/get_app_list")
def get_steam_app_list(search_query: str = None, sort_order: str = "asc"):
    url = "https://api.steampowered.com/ISteamApps/GetAppList/v2/"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        apps = data["applist"]["apps"]
        apps = [app for app in apps if app["name"].strip() != ""]

        def sorting_key(app):
            app_name = app["name"]
            index = next(
                (i for i, char in enumerate(app_name) if char.isalnum()),
                len(app_name),
            )
            key = app_name[index:]
            return key if sort_order == "asc" else key[::-1]

        apps.sort(key=sorting_key)
        app_dict = {app["appid"]: app["name"] for app in apps}
        if search_query:
            filtered_apps = {
                appid: app_name
                for appid, app_name in app_dict.items()
                if search_query.lower() in app_name.lower()
            }
        else:
            filtered_apps = app_dict

        app_list = [
            {"app_id": appid, "app_name": app_name}
            for appid, app_name in filtered_apps.items()
        ]
        return {"app_list": app_list}
    else:
        return {
            "error": f"Failed to retrieve data. Status code: {response.status_code}"
        }


@app.get("/get_app_details/{app_id}")
def get_steam_app_details(app_id: int):
    url = f"https://store.steampowered.com/api/appdetails?appids={app_id}&key={STEAM_API_KEY}"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        app_details = data.get(str(app_id), {})
        return {"app_details": app_details}
    else:
        return {
            "error": f"Failed to retrieve app details. Status code: {response.status_code}"
        }

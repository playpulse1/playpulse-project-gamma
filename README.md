## Francis Belza, David Sweeney, Kai Martin

## Functionality

- **Game Listing**: This project leverages the Steam API to provide a listing of games, allowing users to search for games, sort them alphabetically or in reverse alphabetical order, and view detailed information about each game.

- **Game Details**: Users can access detailed information about a specific game, including its name, header image, price, and a description. This information is sourced from the Steam API.

- **Authentication**: Users can create accounts and/or login. Once a user is authenticated, they will have access to a wide range of Steam application as well as have the ability to log out.

## Use-Cases and User Stories/Scenarios

- **Scenario 1: Searching for Games**

  - _Given_ a user is on the main page
  - _When_ they enter a search query
  - _Then_ the system displays a list of games matching the query

- **Scenario 2: Sorting Games**

  - _Given_ a user is on the main page
  - _When_ they choose a sorting option (alphabetical or reverse alphabetical)
  - _Then_ the system reorders the game list accordingly

- **Scenario 3: Viewing Game Details**

  - _Given_ a user is on the main page
  - _When_ they click on a game
  - _Then_ the system redirects them to a dedicated page showing detailed information about the selected game

- **Scenario 4: Logging In and/or Signing Up to View Games**
  - _Given_ a user is on either the login or signup page
  - _When_ they input their credentials and either click the "login" or "signup" button
  - _Then_ the system redirects them to the main page with a list of games displayed

## Intended Market

This application is aimed at gamers and Steam enthusiasts who want to explore, search, and discover new games available on the Steam platform. It's designed to cater to both casual gamers and serious gaming enthusiasts.

## Stretch Goals

# Frontend Stretch Goal: Display User Profile Information

**Issue Overview:**
This issue is dedicated to enhancing the user experience by displaying the user's first name and username on their profile page, making it more user-friendly and allowing users to easily access their profile information.

**Description:**
In this stretch goal, we aim to create a user profile page layout, retrieve the user's first name and username from the backend API, and display this information on the profile page. The primary goal is to improve user interaction and engagement by offering a visually appealing and user-friendly profile view.

**Success Criteria:**
Once implemented, users should be able to access their profile information on the profile page, where the displayed information is accurate and presented in an aesthetically pleasing format.

---

# Backend Stretch Goal: Serve User Profile Data Securely

**Issue Overview:**
This issue focuses on enhancing data security and privacy by enabling the secure retrieval and serving of user profile data, specifically the user's first name and username, from the database through appropriate authentication and authorization mechanisms.

**Description:**
In this backend stretch goal, we are creating API endpoints to retrieve user profile data, implementing authentication and authorization measures to protect user data, and ensuring the secure retrieval of the user's first name and username from the database. This effort is aimed at maintaining the confidentiality and trustworthiness of user data.

**Success Criteria:**
Upon completion, users should be able to securely access their profile information, with data retrieval from the database being safeguarded against unauthorized access, and the system should include effective error handling and data validation.

## Onboarding

Welcome to Project PlayPulse! Here's a quick guide to help you get started with the local development setup, our branching strategy, managing environment variables (`.env`), handling third-party API keys


- **Local Development Setup:**

  - Fork the repository to your GitHub account.
  - Clone the forked repository locally using `git clone`.
  - Install project dependencies by running `npm install` or `yarn install`, depending on your package manager.
  - Create a feature branch for your work using `git checkout -b feature/your-feature-name`.

- **Branching Strategy: Feature Development Workflow**

  - Our team follows a feature development workflow that allows us to collaborate and maintain a clean and organized codebase. Here's how it works:
  - Each team member creates an individual branch for their work. This branch is where you'll develop and test your feature or fix.
  - While working on your individual branch, make your changes and implement new features or fixes.
  - Once your individual work is ready for testing, merge your changes into the `testing` branch. This branch serves as a staging area where we collectively validate and test each feature.
  - Our team collaborates and tests features in the `testing` branch to ensure they work well together.
  - When the features in the `testing` branch are thoroughly tested and ready, we create pull requests to merge the `testing` branch into the `main` branch.
  - By following this workflow, we maintain a clean and organized `main` branch that only contains well-tested and verified features. Individual work is done in separate branches, which keeps our development process efficient and minimizes conflicts.

- **Environment Variables (`.env`) Setup**

  - To manage environment variables for your project, we provide an `.env.sample` file that serves as a template for required configuration settings. Here's how to set up your environment variables:
  - Start by copying the `.env.sample` file located in the root of your project.
  - Rename the copied file to `.env` (e.g., `cp .env.sample .env`).
  - Open the `.env` file in your preferred text editor.
  - Fill in the required environment variables with the appropriate values. We utilize a Steam API key so the environment variables required should be REACT_APP_API_HOST, STEAM_API_KEY, and SIGNING_KEY

- **Third-Party API Integration (Steam API)**
  - Our project integrates with Steam's third-party API to provide specific functionalities. If you plan to utilize this API in your project or if you're a new user, here's how to manage API keys:
  - To utilize the Steam API, you need to obtain your own Steam API key. If you don't have one, you can sign up and get your API key from the [Steam Developer portal](https://steamcommunity.com/dev/apikey).
  - Once you have your Steam API key, store it in your `.env` file using the `STEAM_API_KEY` variable.

## Tech Stack

- **Frontend**:

  - React: The user interface is built using React, providing a responsive and interactive user experience.

- **Backend**:

  - FastAPI: The backend server is powered by FastAPI, which offers a fast and efficient framework for creating APIs.

- **Database**:

  - MongoDB: While not explicitly mentioned in the provided code, MongoDB can be integrated to store additional data or user-related information.

- **Additional Tools**:
  - MongoExpress: A web-based MongoDB admin interface that can be used for database management.

This technology stack enables the project to provide a seamless and responsive gaming experience while leveraging the Steam API for game data retrieval.

## Journaling

- We maintain a collection of journals that document our project's development journey, challenges, and achievements. You can access these journals in the "journals" folder located in the project's root directory. The journals offer insights into our project's progress and decision-making process.
- [View Journals](./journals/)
- Feel free to explore our project's journals to get a better understanding of the development process and to follow our team's progress. Each journal entry provides a snapshot of our journey and the lessons learned along the way.

## Documentation

- We've created a wireframe to help visualize the project's structure and design. You can access our wireframe using Excalidraw by following this link:
  [View Wireframe on Excalidraw](https://excalidraw.com/#room=21954dc22890dc29b30b,UfalczvM-U56fbUbnMVfUw)
- The wireframe provides an overview of our project's layout and user interface. It's a valuable resource for understanding the project's design and flow. If you have any questions or need further clarification, please refer to the wireframe or reach out to our team for assistance.

We've also created an ideation Google document to help start to imagine our project's structure and design. You can access our document using Google Docs by following this link:
  [View Ideation on Google Docs](https://docs.google.com/document/d/19oGdDYT45-qJQ5mBAVTiySF84rUQF2PD-xCRlEjcFsI/edit?usp=sharing)

## Issue Tracking

- We manage our project's issues and development tasks on GitLab. You can find and track the progress of individual issues and features by visiting our project's issue tracker:
  [View Project Issues on GitLab](https://gitlab.com/playpulse1/playpulse-project-gamma/-/issues)
- In our issue tracker, you'll find a comprehensive list of tasks, feature requests, and bug reports. Feel free to explore, contribute, and provide feedback to help us improve the project. If you encounter any issues or have suggestions, please create a new issue on GitLab, and we'll address it promptly.

## Testing

- **Kai's Unit Test(s) : (Main.test.js)**
  My React unit test verifies that the "Main" component renders successfully in a testing environment. It helps ensure that the Main.js component is functioning as expected and doesn't throw any errors during rendering.

- **Francis' Unit Test(s) : (playpulse-project-gamma\api\tests\test_accounts.py)**
  Run "python -m pytest" in the fastapi docker container to run the test.

- **David's Unit Test(s) : (playpulse-project-gamma\api\tests\testmainapi.py)**
  Run "python -m unittest testmainapi" in the testmainapi.py file to run the test.

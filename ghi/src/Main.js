import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./index.css";

function Main() {
  const { token } = useToken();
  const [games, setGames] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [sortOrder, setSortOrder] = useState("asc");
  const gamesPerPage = 20;
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    fetch(`http://localhost:8000/get_app_list?sort_order=${sortOrder}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => setGames(data.app_list))
      .catch((error) => console.error(error));
  }, [sortOrder]);

  const handleSearchInputChange = (e) => {
    setSearchQuery(e.target.value);
    setCurrentPage(1);
  };
  const handleSortChange = (e) => {
    setSortOrder(e.target.value);
    setCurrentPage(1);
  };
  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const filteredGames = games.filter((game) =>
    game.app_name.toLowerCase().includes(searchQuery.toLowerCase())
  );
  const sortedGames = [...filteredGames];
  if (sortOrder === "asc") {
    sortedGames.sort((a, b) => a.app_name.localeCompare(b.app_name));
  } else if (sortOrder === "desc") {
    sortedGames.sort((a, b) => b.app_name.localeCompare(a.app_name));
  }
  const currentGames = filteredGames.slice(indexOfFirstGame, indexOfLastGame);
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  if (!token) {
    return (
      <div id="center" className="px-4 py-5 my-5 text-center">
        <h1 id="centerplay" className="display-5 fw-bold">
          PlayPulse
        </h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Please log in or sign up to access this page.
          </p>
        </div>
      </div>
    );
  }

  return (
    <div id="center" className="px-4 py-5 my-5 text-center">
      <h1 id="centerplay" className="display-5 fw-bold">
        PlayPulse
      </h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Browse and review all of the latest Steam apps!
        </p>
      </div>
      <input
        type="text"
        className="search-bar"
        placeholder="Search for games"
        value={searchQuery}
        onChange={handleSearchInputChange}
      />
      <div id="logdiv" className="sort-container">
        <label className="sort-label" htmlFor="sortDropdown">
          Sort by:
        </label>
        <select
          id="sortDropdown"
          className="dropdown-content"
          onChange={handleSortChange}
        >
          <option value="asc">Alphabetical</option>
          <option value="desc">Reverse Alphabetical</option>
        </select>
      </div>
      <div className="game-list-container bubble">
        <ul className="game-list">
          {currentGames.map((game) => (
            <li key={game.app_id} className="game-list-item">
              <Link to={`/app/${game.app_id}`}>{game.app_name}</Link>
            </li>
          ))}
        </ul>
      </div>
      <div className="pagination">
        <button
          onClick={() => paginate(currentPage - 1)}
          disabled={currentPage === 1}
        >
          Previous
        </button>
        <button
          onClick={() => paginate(currentPage + 1)}
          disabled={currentGames.length < gamesPerPage}
        >
          Next
        </button>
      </div>
    </div>
  );
}

export default Main;

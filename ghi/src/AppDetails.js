import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "./index.css";

function AppDetails() {
  const [appDetails, setAppDetails] = useState({});
  const { appId } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `http://localhost:8000/get_app_details/${appId}`
        );
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        setAppDetails(data.app_details);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, [appId]);

  const hasData = appDetails && appDetails.success;
  const appData = appDetails.data;

  const getAppPrice = (priceOverview) => {
    if (priceOverview && priceOverview.final_formatted) {
      return priceOverview.final_formatted;
    }
    return "Price not available";
  };
  const renderHTML = (html) => {
    const modifiedHtml = html
      .replace(/<h1/g, "<h3")
      .replace(/<\/h1>/g, "</h3>")
      .replace(/<h2/g, "<h3")
      .replace(/<\/h2>/g, "</h3>");
    return { __html: modifiedHtml };
  };

  return (
    <div>
      {hasData ? (
        <div>
          <h1 id="centerplay">{appData.name}</h1>
          <img id="img1" src={appData.header_image} alt={appData.name} />
          <p>
            <h2>Price:</h2> {getAppPrice(appData.price_overview)}
          </p>
          <p>
            <h2>Description:</h2>{" "}
            <div
              id="img"
              dangerouslySetInnerHTML={renderHTML(appData.detailed_description)}
            />
          </p>
        </div>
      ) : (
        <p>App details not available</p>
      )}
    </div>
  );
}

export default AppDetails;

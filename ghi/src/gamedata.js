import './index.css';

import { useState, useEffect } from "react";

  const [gameName, setGameName] = useState("");
  const [gameDescription, setGameDescription] = useState("");

  useEffect(() => {
    const gameData = {
      app_details: {
        success: true,
        data: {
          name: "The Great Escape",
          detailed_description: "Your game description here",
        },
      },
    };
    setGameName(gameData.app_details.data.name);
    setGameDescription(gameData.app_details.data.detailed_description);
  }, []);

  return (
    <div className="card text-bg-light mb-3">
      <h5 className="card-header">Signup</h5>
      <div className="card-body">
        <form onSubmit={(e) => handleRegistration(e)}>
          <div className="mb-3">
            <label className="form-label">Game Name</label>
            <input
              name="gameName"
              type="text"
              className="form-control"
              value={gameName}
              readOnly
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Game Description</label>
            <textarea
              name="gameDescription"
              className="form-control"
              value={gameDescription}
              readOnly
            />
          </div>
          <div>
            <input className="btn btn-primary" type="submit" value="Register" />
          </div>
        </form>
      </div>
    </div>
  );
};

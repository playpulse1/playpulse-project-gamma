import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    login(username, password);
    navigate("/");
  };

  return (
    <div id="centerlogs" className="card text-bg-light mb-3">
      <h5 id="logsize" className="card-header">
        Login
      </h5>
      <div className="card-body">
        <form onSubmit={(e) => handleSubmit(e)}>
          <div id="logdiv" className="mb-3">
            <label id="loglabl" className="form-label">
              Username:
            </label>
            <input
              name="username"
              type="text"
              className="form-control"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label id="loglabl" className="form-label">
              Password:
            </label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <input
              id="loglabl"
              className="btn btn-primary"
              type="submit"
              value="Login"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginForm;
